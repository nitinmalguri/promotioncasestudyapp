import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
const logger = createLogger({
  // ...options
});

import reducer from '../reducers';// All redux reducers (rolled into one mega-reducer)

// Load middleware
let middleware = [
  thunk, // Allows action creators to return functions (not just plain objects)
];

if (__DEV__) {
  // Dev-only middleware
  middleware = [
    ...middleware,
    logger, // Logs state changes to the dev console
  ];
}

// Init redux store (using the given reducer & middleware)
export default configureStore = () => {
  return compose(
    applyMiddleware(...middleware)
  )(createStore)(reducer);
}
