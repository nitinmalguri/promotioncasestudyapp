/**
 * #GlobalReducers.js
 *
 * Reducers for the global action.
 *
 */

'use strict'

const InitialState = require('./initialState').default;
const initialState = new InitialState();
import * as t from '../actionTypes';


/**
 * Main reducers to handle all the actions of Glbal action.
 *
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */

export default function loginReducer(state = initialState, action) {
    switch (action.type) {
        case t.LOGIN_SUCCESS:
        var user = action.user;
        var refreshToken = user.refreshToken;
        AsyncStorage.setItem('refreshToken', refreshToken);
        var userStr = JSON.stringify(user)
        AsyncStorage.setItem('user', userStr);
            return state.setIn(['login', 'user'], userStr);
        case t.LOGGED_OUT:
            return state.setIn(['login', 'user'], null);

    }// switch
    /**
     * # Default
     */
    return state;
}
