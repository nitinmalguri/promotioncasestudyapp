import * as t from '../actionTypes';
import * as API from "../../../common/api/api";
import { Actions } from 'react-native-router-flux';

/**
 * This will register user to the store.
 *
 */
export function logInSucess(user) {
    return {
        type: t.LOGIN_SUCCESS,
        payload: user
    }
}

/**
* This will pull out user out of the store.
 *
 */
export function logOutSucess(user) {
    return {
        type: t.LOGGED_OUT,
        payload: null
    }
}

export function register(data, successCB, errorCB) {
    return (dispatch) => {
        API.createUserWithEmailAndPassword(data, function (success, user, error) {
            if (success) {
                successCB();
            }else if (error) {
              errorCB(error)
            }
        });
    };
}

export function login(data,successCB, errorCB) {
    return (dispatch) => {
        API.signInWithEmailAndPassword(data, function (success, data, error) {
            if (success) {
                successCB();
            }else if (error) {
              errorCB(error)
            }
        });
    };
}


export function updateUser(userId, data, successCB, errorCB) {
    return (dispatch) => {
        API.updateUser(userId, data, function (succss, data, error) {
            if (success) successCB();
            else if (error) errorCB(error)
        });
    };
}

export function signOut(successCB, errorCB) {
    return (dispatch) => {
        API.signOut(function (success, data, error) {
            if (success) {
                dispatch(logOutSucess());
                successCB();
            }else if (error) errorCB(error)
        });
    };
}
