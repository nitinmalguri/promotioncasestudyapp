'use strict';

import { Record } from 'immutable';

const Login = Record({
  user: null,
});

var InitialState = Record({
  login: new Login(),
});

export default InitialState;
