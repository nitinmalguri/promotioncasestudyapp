import { combineReducers } from 'redux'

import Global from './global/globalReducer';
import Login from './login/loginReducer';

const appReducer = combineReducers({
  Global,
  Login
});

const rootReducer = (state, action) => {
  return appReducer(state, action)
}

module.exports = rootReducer;
