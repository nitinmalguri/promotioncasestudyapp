export const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS';
export const LOGGED_OUT = 'auth/LOGGED_OUT';
export const NETWORK_LOADING_REQUEST = 'global/NETWORK_LOADING_REQUEST'
export const GET_DEALS = 'global/GET_DEALS'
export const GET_CATEGORIES = 'global/GET_CATEGORIES'
