/**
 * ## Imports
 * actions for the global actions.
 */

 import DealModel from "./dealModel"
 import * as API from "../../../common/api/api";
 import * as t from '../actionTypes';

/**
 * This will return the action for the loading request.
 *
 */
export function showNetworkLoading(showNetworkLoading) {
    return {
        type: t.NETWORK_LOADING_REQUEST,
        payload: showNetworkLoading
    }
}

/**
 * This will return the deals.
 *
 */
export function gotDeals(deals) {
    return {
        type: t.GET_DEALS,
        payload: deals
    }
}

/**
 * This will return the category.
 *
 */
export function gotCategories(categories) {
    return {
        type: t.GET_CATEGORIES,
        payload: categories
    }
}

/**
 * this will download all the deals from server.
 * And will update the local firebase instance
 */
export function downloadDeals() {
  return dispatch => {
    API.getDeals(function (data) {
        var processedData = DealModel.getProcessedData(data)
        dispatch(gotDeals(processedData.deals ? processedData.deals : null));
        dispatch(gotCategories(processedData.categories ? processedData.categories : null));
        dispatch(showNetworkLoading(false));
        });
  }
}
