'use strict';

const {Record} = require('immutable');

const Global = Record({
  showNetworkLoading: true,
  error:null,
  deals: [],
  categories:[],
});

var InitialState = Record({
  global: new Global(),
});

export default InitialState;
