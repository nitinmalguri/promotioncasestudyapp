/**
 * #GlobalReducers.js
 *
 * Reducers for the global action.
 *
 */

'use strict'

const InitialState = require('./initialState').default;
const initialState = new InitialState();
import * as t from '../actionTypes';


/**
 * Main reducers to handle all the actions of Glbal action.
 *
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */

export default function globalReducer(state = initialState, action) {
    switch (action.type) {
        case t.NETWORK_LOADING_REQUEST:
            return state.setIn(['global', 'showNetworkLoading'], action.payload);
        case t.GET_DEALS:
            return state.setIn(['global', 'deals'], action.payload);
        case t.GET_CATEGORIES:
          return state.setIn(['global', 'categories'], action.payload);
    }// switch
    /**
     * # Default
     */
    return state;
}
