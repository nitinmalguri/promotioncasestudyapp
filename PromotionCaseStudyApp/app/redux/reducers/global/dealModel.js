
/**
 * Global Data processing Functions
 */
'use strict';

class DealModel {

  /**
   * Convent data to required array
   * @param - raw data
   * @returns - Array of categories and deals
   */


  static getProcessedData(data){
    var deals = [];
    var categories = [];
    data.forEach((child) => {
      categories.push(child.key)
      child.val().forEach((deal) => {
        deals.push({
          lat: deal.lat,
          lng: deal.lng,
          id: deal.id,
          name: deal.name,
          rating: deal.rating,
          type: deal.type,
          discount: deal.discount,
          vicinity: deal.vicinity,
          minCost: deal.minCost,
          date: deal.date,
          brief: deal.brief,
          description: deal.description,
          services: deal.services,
        });
      });
    });
    return {"categories":categories,"deals":deals}
  }
}

module.exports = DealModel;
