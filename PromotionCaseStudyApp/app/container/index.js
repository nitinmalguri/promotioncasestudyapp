/**
 * Index - this is where everything
 *  starts - but offloads to app.js
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';

import configureStore from '../redux/store';
import App from './app';

// Wrap App in Redux provider (makes Redux available to all sub-components)
export default class AppContainer extends Component {
  render() {
    const store = configureStore();
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}
