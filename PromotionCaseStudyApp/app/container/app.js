'use strict';

/* imports ==================================================================== */
import React, { Component } from 'react'
import { StatusBar} from 'react-native'

import {Scene, Router} from 'react-native-router-flux';
import Dummy from './AppDummy';

// Screens
// import Home from '../screens/home';
// import Login from '../screens/login';
// import Registration from '../screens/registration';
// import Category from '../screens/category';
// import Details from '../screens/details';


export default class AppContainer extends Component {

  componentDidMount = () => {
    // Status Bar
    StatusBar.setHidden(false, 'slide'); // Slide in on load
  }

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene initial animation='fade' key="Dummy" component={Dummy} hideNavBar />
        </Scene>
      </Router>
    );
  }
}
